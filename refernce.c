#include<stdio.h>
void swap_callby_ref(int*,int*);
int main()
{
int a,b;
printf("enter the values");
scanf("%d%d",&a,&b);
printf("in main fuction ,a=%d&b=%d",a,b);
swap_callby_ref(&a,&b);
printf("in main fuction,a=%d&b=%d",a,b);
return 0;
}
void swap_callby_ref(int*a,int *b)
{
int temp;
temp=*a;
*a=*b;
*b=temp;
printf("in fun(call_by_ref)a=%d and b=%d",*a,*b);
}